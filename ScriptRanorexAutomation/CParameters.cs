﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScriptRanorexAutomation
{
    public class CParameters
    {
       public string EmailParameter { get; set; }
       public string Enviroment { get; set; } 


       public CParameters()
       {
            EmailParameter = "automation.malaga@bestseller.com";
       }   
        
       public string ChangeEmailParameter()
       {
           Console.WriteLine("Enter the email");
           EmailParameter = Console.ReadLine();
           return EmailParameter;
       }

       public string ChangeEnviroment()
        {
            Console.WriteLine("Select the environment");
            EmailParameter = Console.ReadLine();
            return Enviroment;
        }
        
        public void ListParametersDefault()
        {
            Console.WriteLine("-----------DEFAULT PARAMETERS-----------");
            Console.WriteLine("Email: "+EmailParameter);
            Console.WriteLine("Enviroment: " + Enviroment);
            Console.WriteLine();
            Console.WriteLine();
        }

        public string GetScenarioPath(string _xmlPath)
        {
            string path="";

            XmlDocument doc = new XmlDocument();
            doc.Load(@"C:\Users\jorge.fuente\Desktop\path.xml");

            XmlNodeList xParentPath = doc.GetElementsByTagName(_xmlPath);
            XmlNodeList xScenarioPath = ((XmlElement)xParentPath[0]).GetElementsByTagName("path");
            

            foreach (XmlElement nodo in xScenarioPath)
            {
                path = nodo.GetAttribute("name");
                Console.WriteLine(path);
               
            }
            
            return path;
        }    }
}
