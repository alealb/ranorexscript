﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ScriptRanorexAutomation
{
    public class CTestSuite
    {
        public string TcSuiteName;
        public int TcSuiteNumber { get; set; }
        public int TSuiteNumber { get; set; }
        

        private FileInfo[] TestSuiteDirectoryMethod()
        {
            String mydir = @"C:\Users\jorge.fuente\OneDrive - BESTSELLER\finance_e2e\Finance_e2e\bin\Debug";

            int fCount = Directory.GetFiles(mydir, "*.rxtst", SearchOption.AllDirectories).Length;

            DirectoryInfo d = new DirectoryInfo(@"C:\Users\jorge.fuente\OneDrive - BESTSELLER\finance_e2e\Finance_e2e\bin\Debug");

            FileInfo[] Files = d.GetFiles("*.rxtst");

            return Files;
        }
        
        public List<CTestSuite> TestSuiteListMethod()
        {
            FileInfo[] Files = TestSuiteDirectoryMethod();

            List<CTestSuite> testSuites = new List<CTestSuite>();

            foreach (FileInfo file in Files)
            {
                CTestSuite suite = new CTestSuite
                {
                    TcSuiteName = file.Name,
                    TcSuiteNumber = TSuiteNumber
                };
                testSuites.Add(suite);
                TSuiteNumber++;
            }

            return testSuites;
        }

        public void PrintTestSuiteOptionMethod(List<CTestSuite> testsuites)
        {
            Console.WriteLine("---------------SUITES AVAILABLE---------------");
            foreach (var element in testsuites)
            {                
                Console.WriteLine("Option number {0} : {1}", element.TcSuiteNumber, element.TcSuiteName);
               
            }
            Console.WriteLine("----------------------------------------------");
        }

    }
}
